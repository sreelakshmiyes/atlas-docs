# Submit digicollect mandate

> This API is for to submit digicollect mandate for loan repayment collection

---

### GRAPHQL

---

## Headers

| Fields         | Type   | Description                 | Example                                         | Data Management|
| -------------- | ------ | --------------------------- | ------------------------------------------------|--------------- |
| URL            | string |                             | `{base_url}/graphql/fineract-interface-service` |                |
| GRAPHQL        | string |                             | **MUTATION**                                    |                |
| content-type   | string | JSON                        | **application/json**                            |                |
| x-access-token | string | session token with validity | **token**                                       | user token     |

---

## Request

```json
mutation
{
    submit_digicollect_mandate(loan_id:"523",client_id:"163")
    {
        mandate_id,
        state,
        type,
        bank_details
        {
            shared_with_bank,
            bank_name,
            state
        },
        created_at,  
        service_provider_details
        {
            service_provider_name,
            service_provider_utility_code
        },
        access_token
        {
            created_at,
            id,
            entity_id,
            valid_till
        }
    }   
}
```
---

## Request Parameters

| Fields    | Type   | Description                  | Required                      |
| --------- | ------ | -----------------------------| ----------------------------- |
| loan_id   | string | loan application id          |  <div align="center">✔</div>  |
| client_id | string | client id                    |  <div align="center">✔</div>  |

---
## Success

```json
{
    "data": {
        "submit_digicollect_mandate": {
            "mandate_id": "ENA2106161458004984XDX542VKPCOAP",
            "state": "partial",
            "type": "CREATE",
            "bank_details": {
                "shared_with_bank": "dltbgcmse-collproduct@yesbank.in",
                "bank_name": "Yes Bank Ltd",
                "state": "partial"
            },
            "created_at": true,
            "service_provider_details": {
                "service_provider_name": "Mad Over Mobile Private Limited",
                "service_provider_utility_code": "NACH00000000021717"
            },
            "access_token": {
                "created_at": "2021-06-16 14:58:00",
                "id": "GWT210616145800541I3JDICOT459AK9",
                "entity_id": "ENA2106161458004984XDX542VKPCOAP",
                "valid_till": "2021-06-17 14:58:00"
            }
        }
    }
}

```
---

## Error

```json
{
  "errors": [
    {
      "statusCode": 500,
      "message": "Error in submitting mandate info for repayment collection",
      "locations": [
        {
          "line": 3,
          "column": 3
        }
      ],
      "path": [
        "submit_digicollect_mandate"
      ]
    }
  ],
  "data": {
    "submit_digicollect_mandate": null
  }
}
```
---
