# Entry Client Work Details

> This API is for to store client work details

---

### GRAPHQL

---

## Headers

| Fields         | Type   | Description                 | Example                                        | Data Management |
| -------------- | ------ | --------------------------- | ---------------------------------------------- | --------------- |
| URL            | string |                             | `{base_url}/graphql/fineract-interface-service`|                 |
| GRAPHQL        | string |                             | **MUTATION**                                   |                 |
| content-type   | string | JSON                        | **application/json**                           |                 |
| x-access-token | string | session token with validity | **token**                                      | user token      |

---

## Request

```json
mutation
{
  entry_client_work_details( 
    employerName:"Kred",
    monthlySalary:20000)
    {  
      resourceId     
    }
  
}


```

---

## Request Parameters

| Fields              | Type   | Description                  | Required                    |
| ------------------- | ------ | ---------------------------- | --------------------------- |
| employerName        | string | employerName  of client      | <div align="center">✔</div> |
| monthlySalary       | integer| monthlySalary of client      | <div align="center">✔</div> |


---

## Success

```json
{
  "data": {
    "entry_client_work_details": {
      "resourceId": "602d19f08eeb8a2bf486b33d"
      
    }
  }
}




```

---

## Error

```json
{
  "errors": [
    {
      "statusCode": 500,
      "message": "Error in storing client work details",
      "locations": [
        {
          "line": 3,
          "column": 3
        }
      ],
      "path": [
        " entry_client_work_details"
      ]
    }
  ],
  "data": {
    " entry_client_work_details": null
  }
}
```

---
