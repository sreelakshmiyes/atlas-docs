# Get cbs user profile 

> This API is for to load client profile

---

### GRAPHQL

---

## Headers

| Fields         | Type   | Description                 | Example                                        | Data Management |
| -------------- | ------ | --------------------------- | -----------------------------------------------| --------------- |
| URL            | string |                             | `{base_url}/graphql/fineract-interface-service`|                 |
| GRAPHQL        | string |                             | **QUERY**                                      |                 |
| content-type   | string | JSON                        | **application/json**                           |                 |
| x-access-token | string | session token with validity | **token**                                      | user token      |

---

## Request

```json
query{get_cbs_user_profile{
 
  firstname, 
  lastname, 
  gender, 
  mobile,
  dateofbirth,
  email, 
  company_id,
  pan, 
  aadhaar, 
  employer_name,
  monthly_salary, 
  success, 
  credit_score,
  credit_score_percentage
}
  
}
```

---


## Success

```json
{
  "data": {
    "get_cbs_user_profile": {
      "firstname": "Preshin",
      "lastname": "P S",
      "gender": "Male",
      "mobile": "7012297109",
      "dateofbirth": "23-12-1993",
      "email": "preshinsmith@gmail.com",
      "company_id": "59de2820f31cc90001d65bb0",
      "pan": "ABCDE1234F",
      "aadhaar": "231546879021",
      "employer_name": "Atlas",
      "monthly_salary": 20000,
      "success": true,
      "credit_score": "600",}
      "credit_score_percentage": "70"
    }
  }
}

```

---

## Error

```json
{
  "errors": [
    {
      "statusCode": 500,
      "message": "Error in fetching CBS user profile",
      "locations": [
        {
          "line": 3,
          "column": 3
        }
      ],
      "path": [
        "get_cbs_user_profile"
      ]
    }
  ],
  "data": {
    "get_cbs_user_profile": null
  }
}
```

---
