# Self Sign Up Client

> This API is for to sign up the client

---

### GRAPHQL

---

## Headers

| Fields         | Type   | Description                 | Example                                        -| Data Management |
| -------------- | ------ | --------------------------- | ----------------------------------------------- | --------------- |
| URL            | string |                             | `{base_url}/graphql/client-registration-service`|                 |
| GRAPHQL        | string |                             |        **MUTATION**                             |                 |
| content-type   | string | JSON                        |       **application/json**                      |                 |

---

## Request

```json
mutation
{
  self_signup_cbs_account_v2(
    firstname:"Lalitha",
    lastname:"V N",
    gender:  "female",
    dateofbirth:"1973-04-24",
    mobile:"+919387211575",
    email:"lalithavn@gmail.com",
    company_id:"5fbcb4821ffc64855233b1b1",
    aadhaar:"1122334455667",
    employer_name:"Kred",
    monthly_salary:15000)
    {
      clientId,
      success
    }
}



```

---

## Request Parameters

| Fields          | Type    | Description                | Required                     |
| --------------- | ------- | ---------------------------| -----------------------------|
|  firstname      | string  |       firstname            |  <div align="center">✔</div> |
|  lastname       | string  |       lastname             |  <div align="center">✔</div> |
|  gender         | string  |       gender               |  <div align="center">✔</div> |
|  dateofbirth    | string  |       dateofbirth          |  <div align="center">✔</div> |
|  mobile         | string  |       mobile               |  <div align="center">✔</div> |
|  email          | string  |       email                |  <div align="center">✔</div> |
|  company_id     | string  |       company_id           |  <div align="center">✔</div> |
|  aadhaar        | string  |       aadhaar              |  <div align="center">✔</div> |
|  employer_name  | string  |       employer_name        |  <div align="center">✔</div> |
|  monthly_salary | integer |       monthly_salary       |  <div align="center">✔</div> |

---

## Success

```json
{
  "data": {
    "self_signup_cbs_account_v2": {
      "clientId": 161,
      "success": true
    }
  }
}




```

---

## Error

```json
{
  "errors": [
    {     
      "message": "Error in signing up CBS user",
      "locations": [
        {
          "line": 3,
          "column": 3
        }
      ],
      "path": [
        "self_signup_cbs_account_v2"
      ]
    }
  ],
  "data": {
    "self_signup_cbs_account_v2": null
  }
}
```

---
