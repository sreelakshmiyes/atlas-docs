# Client Retrieve Document Status

> This API is for to retrieve client document upload status

---

### GRAPHQL

---

## Headers

| Fields         | Type   | Description                 | Example                                        | Data Management |
| -------------- | ------ | --------------------------- | -----------------------------------------------| --------------- |
| URL            | string |                             | `{base_url}/graphql/fineract-interface-service`|                 |
| GRAPHQL        | string |                             | **QUERY**                                      |                 |
| content-type   | string | JSON                        | **application/json**                           |                 |
| x-access-token | string | session token with validity | **token**                                      |   user token    |

---

## Request

```json
query{
  client_retrieve_document_status
  { 
    aadhar_back,
    aadhar_front,
    pan 
  }
}
```
---

## Success

```json
{
  "data": {
    "client_retrieve_document_status": {
  	  "aadhar_back": true,
  	  "aadhar_front": true,
  	  "pan": true
	  }
  }
}

```

---

## Error

```json
{
  "errors": [
    {
      "statusCode": 500,
      "message": "Error in fetching CBS user's document upload status",
      "locations": [
        {
          "line": 3,
          "column": 3
        }
      ],
      "path": [
        "client_retrieve_document_status"
      ]
    }
  ],
  "data": {
    "client_retrieve_document_status": null
  }
}
```

---
