# Update User Profile 

> This API is for to update client/user profile

---

### GRAPHQL

---

## Headers

| Fields         | Type   | Description                 | Example                                         | Data Management|
| -------------- | ------ | --------------------------- | ------------------------------------------------|--------------- |
| URL            | string |                             | `{base_url}/graphql/fineract-interface-service` |                |
| GRAPHQL        | string |                             | **MUTATION**                                    |                |
| content-type   | string | JSON                        | **application/json**                            |                |
| x-access-token | string | session token with validity | **token**                                       | user token     |

---

## Request

```json
mutation{
  update_cbs_user_profile(
  email:"sreelakshmiyes@gmail.com",
  company_id:"5fbcb4821ffc64855233b1b1", 	
  firstname:"Sreelakshmi ",
  lastname:"S",
  gender:"female",
  mobile:"7107969017",
  dateofbirth:"1986-01-06", 
  pan:"BTKPS9952F",
  aadhaar:"781243167818",
  employer_name:"Kred Ltd",
  monthly_salary:25000)
  { 
    success,
    client_Id 
  }
}
```
---

## Success

```json
{
  "data": {
	"update_cbs_user_profile": {
  	"success": true,
  	"client_Id": "105"
	}
  }
}

```

---

## Error

```json
{
  "errors": [
    {
      "statusCode": 500,
      "message": "Error in updating CBS user profile",
      "locations": [
        {
          "line": 3,
          "column": 3
        }
      ],
      "path": [
        "update_cbs_user_profile"
      ]
    }
  ],
  "data": {
    "update_cbs_user_profile": null
  }
}
```

---
