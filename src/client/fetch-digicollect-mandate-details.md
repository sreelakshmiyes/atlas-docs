# Fetch digicollect mandate details

> This API is for to fetch details of mandate submitted by user

---

### GRAPHQL

---

## Headers

| Fields         | Type   | Description                 | Example                                         | Data Management|
| -------------- | ------ | --------------------------- | ------------------------------------------------|--------------- |
| URL            | string |                             | `{base_url}/graphql/fineract-interface-service` |                |
| GRAPHQL        | string |                             | **QUERY   **                                    |                |
| content-type   | string | JSON                        | **application/json**                            |                |
| x-access-token | string | session token with validity | **token**                                       | user token     |

---

## Request

```json
query
{
    fetch_digicollect_mandate_details(loan_id:523,client_id:163)
    {
        customer_name,
        customer_account_number,
        umrn,
        destination_bank_id,
        destination_bank_name,
        mandate_state  
    }
}

```
---

---

## Request Parameters

| Fields    | Type   | Description                  | Required                      |
| --------- | ------ | -----------------------------| ----------------------------- |
| loan_id   | string | loan application id          |  <div align="center">✔</div>  |
| client_id | string | client id                    |  <div align="center">✔</div>  |

---

## Success

```json
{
  "data": 
  {
    "fetch_digicollect_mandate_details": 
    {
        "customer_name": "Sachin T S",
        "customer_account_number": "xxxxxxxx5233",
        "umrn": "UMRN1331132849426623",
        "destination_bank_id": "SBIN0070266",
        "destination_bank_name": "STATE BANK OF INDIA",
        "mandate_state": "auth_success"
    }
  }
}

```
---

## Error

```json
{
  "errors": [
    {
      "statusCode": 500,
      "message": "Error! Not able to fetch mandate information",
      "locations": [
        {
          "line": 3,
          "column": 3
        }
      ],
      "path": [
        "fetch_digicollect_mandate_details"
      ]
    }
  ],
  "data": {
    "fetch_digicollect_mandate_details": null
  }
}
```
---
