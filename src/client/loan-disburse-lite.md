# Loan Disburse Lite

> This API is for to disburse loan to a client's paysack card

---

### GRAPHQL

---

## Headers

| Fields         | Type   | Description                 | Example                                        | Data Management |
| -------------- | ------ | --------------------------- | ---------------------------------------------- | --------------- |
| URL            | string |                             | `{base_url}/graphql/fineract-paysack-payments` |                 |
| GRAPHQL        | string |                             | **MUTATION**                                   |                 |
| content-type   | string | JSON                        | **application/json**                           |                 |
| x-access-token | string | session token with validity | **token**                                      |   user token    |

---

## Request

```json
mutation{
 loans_disburse_lite(
  principal:10,
  loan_term:3,
  no_of_repayments:3,
  product_id:16)
  {
    officeId,
    clientId,
    loanId,
    resourceId,
    changes
      {
        checkNumber,
        status
        {
          id,
          code,
          value,
          pendingApproval,
          waitingForDisbursal,
          active,
          closedObligationsMet,
          closedWrittenOff,
          closedRescheduled,
          closed,
          overpaid
        },
        locale,
        dateFormat,
        actualDisbursementDate
      }    
  }
}
 
```

---

## Request Parameters

| Fields            | Type   | Description                   | Required                    |
| ----------------- | ------ | ----------------------------- | --------------------------- |
| principal         | integer| principal amount of the loan  | <div align="center">✔</div> |
| loan_term         | integer| loan term                     | <div align="center">✔</div> |
| no_of_repayments  | integer| no of repayments              | <div align="center">✔</div> |
| product_id        | integer| loan product id               | <div align="center">✔</div> |


---

## Success

```json
{
  "data": {
    "loans_disburse_lite": {
      "officeId": 1,
      "clientId": "131",
      "loanId": 470,
      "resourceId": 470,
      "changes": {
        "checkNumber": "12345",
        "status": {
          "id": 300,
          "code": "loanStatusType.active",
          "value": "Active",
          "pendingApproval": false,
          "waitingForDisbursal": false,
          "active": true,
          "closedObligationsMet": false,
          "closedWrittenOff": false,
          "closedRescheduled": false,
          "closed": false,
          "overpaid": false
        },
        "locale": "en",
        "dateFormat": "dd MMMM yyyy",
        "actualDisbursementDate": "05 March 2021"
      }
    }
  }
}

```

---

## Error

```json
{
  "errors": [
    {
      "statusCode": 500,
      "message": "Error in disbursing loan",
      "locations": [
        {
          "line": 3,
          "column": 3
        }
      ],
      "path": [
        "loans_disburse_lite"
      ]
    }
  ],
  "data": {
    "loans_disburse_lite": null
  }
}
```


