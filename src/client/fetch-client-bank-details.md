# Fetch Client Bank Details

> This API is for to fetch client bank details

---

### GRAPHQL

---

## Headers

| Fields         | Type   | Description                 | Example                                        | Data Management |
| -------------- | ------ | --------------------------- | ---------------------------------------------- | --------------- |
| URL            | string |                             | `{base_url}/graphql/fineract-interface-service`|                 |
| GRAPHQL        | string |                             | **QUERY**                                      |                 |
| content-type   | string | JSON                        | **application/json**                           |                 |
| x-access-token | string | session token with validity | **token**                                      | user token      |

---

## Request

```json
query
{
  fetch_client_bank_details
  {
    bankIFSCCode,
    bankName,
    bankAccountName,
    accountNo
  }  
}

```
---


## Success

```json
{
  "data": {
    "fetch_client_bank_details": {
      "bankIFSCCode": "HDFC0009106",
      "bankName": "HDFC",
      "bankAccountName": "Preshin P S",
      "accountNo": "50100197142376"
    }
  }
}
}

```

---

## Error

```json
{
  "errors": [
    {
      "statusCode": 500,
      "message": "Error in fetching client bank details",
      "locations": [
        {
          "line": 3,
          "column": 3
        }
      ],
      "path": [
        "fetch_client_bank_details"
      ]
    }
  ],
  "data": {
    "fetch_client_bank_details": null
  }
}
```

---
