# Loan Schedule

> This API is fetch loan schedule of a product,it will give back EMI details in advance for a particular loan product

---

### GRAPHQL

---

## Headers

| Fields         | Type   | Description                 | Example                                         | Data Management |
| -------------- | ------ | --------------------------- | ----------------------------------------------- | --------------- |
| URL            | string |                             | `{base_url}/graphql/fineract-interface-service` |                 |
| GRAPHQL        | string |                             | **QUERY**                                       |                 |
| content-type   | string | JSON                        | **application/json**                            |                 |
| x-access-token | string | session token with validity | **token**                                       |  user token     |

---
## Request
```json
query{
  loan_schedule(
    principal:100000,    
    loanTermFrequency:7,
    loanTermFrequencyType:0,
    numberOfRepayments:7,
    repaymentEvery:1,
    repaymentFrequencyType:0)
    {    
    currency {
    code,
    name,
    decimalPlaces,
    displaySymbol,
    nameCode,
    displayLabel
    },
    loanTermInDays,
    totalPrincipalDisbursed,
    totalPrincipalExpected,
    totalPrincipalPaid,
    totalInterestCharged,
    totalFeeChargesCharged,
    totalPenaltyChargesCharged, 
    totalRepaymentExpected,  
    totalOutstanding,
    periods{
        fromDate,
        dueDate,   
        principalLoanBalanceOutstanding,     
        feeChargesDue,     
        totalOriginalDueForPeriod,
        totalDueForPeriod,     
        totalOutstandingForPeriod,     
        totalActualCostOfLoanForPeriod        
    }     
  }   
}	
```
---

## Success

```json
{
  "data": {
    "loan_schedule": {
      "currency": {
        "code": "INR",
        "name": "Indian Rupee",
        "decimalPlaces": 2,
        "displaySymbol": "₹",
        "nameCode": "currency.INR",
        "displayLabel": "Indian Rupee (₹)"
      },
      "loanTermInDays": 8,
      "totalPrincipalDisbursed": 100000,
      "totalPrincipalExpected": 100000,
      "totalPrincipalPaid": 0,
      "totalInterestCharged": 1000,
      "totalFeeChargesCharged": 0,
      "totalPenaltyChargesCharged": 0,
      "totalRepaymentExpected": 101000,
      "totalOutstanding": 0,
      "periods": [
        {
          "fromDate": null,
          "dueDate": [2020,12,18],
          "principalLoanBalanceOutstanding": 100000,
          "feeChargesDue": 0,
          "totalOriginalDueForPeriod": 0,
          "totalDueForPeriod": 0,
          "totalOutstandingForPeriod": 0,
          "totalActualCostOfLoanForPeriod": 0
        },
        {
          "fromDate": [2020,12,18],
          "dueDate": [ 2020,12,19],
          "principalLoanBalanceOutstanding": 85714.29,
          "feeChargesDue": 0,
          "totalOriginalDueForPeriod": 14428.57,
          "totalDueForPeriod": 14428.57,
          "totalOutstandingForPeriod": 14428.57,
          "totalActualCostOfLoanForPeriod": 142.86
        },
        .....
        {
          "fromDate": [2020,12,25],
          "dueDate": [2020,12,26],
          "principalLoanBalanceOutstanding": 0,
          "feeChargesDue": 0,
          "totalOriginalDueForPeriod": 14428.58,
          "totalDueForPeriod": 14428.58,
          "totalOutstandingForPeriod": 14428.58,
          "totalActualCostOfLoanForPeriod": 142.84
        }
      ]
    }
  }
}
```
---
## Error

```json
{
  "errors": [
    {
      "statusCode": 500,
      "message": "Error in fetching loan schedule",
      "locations": [
        {
          "line": 3,
          "column": 3
        }
      ],
      "path": [
        "loan_schedule"
      ]
    }
  ],
  "data": {
    "loan_schedule": null
  }
}
```

---