# Client Upload Document

> This API is for to upload client documents for identity verification

---

### GRAPHQL

---

## Headers

| Fields         | Type   | Description                 | Example                                        | Data Management |
| -------------- | ------ | --------------------------- | ---------------------------------------------- | --------------- |
| URL            | string |                             | `{base_url}/graphql/fineract-interface-service`|                 |
| GRAPHQL        | string |                             | **MUTATION**                                   |                 |
| content-type   | string | JSON                        | **application/json**                           |                 |
| x-access-token | string | session token with validity | **token**                                      | user token      |

---

## Request

```json
mutation{
  client_upload_document(
  name:"pancard",
  file:"data:image/jpeg;base64,/9j/4AAQSkZJRgABA…..)
    {
      image_url,
      success 
    }   
}

```

---

## Request Parameters

| Fields   | Type   | Description                  | Required                     |
| -------- | ------ | -----------------------------| ---------------------------- |
| name     | string | name of the file             |  <div align="center">✔</div> |
| file     | string | file                         |  <div align="center">✔</div> |

---

## Success

```json
{
  "data": {
	"client_upload_document": {
  	"image_url": "https://fineract-atlas.s3.amazonaws.com/105/pan",
      "success": true
	   }  
  }
}

```

---

## Error

```json
{
  "errors": [
    {
      "statusCode": 500,
      "message": "Error in client upload document",
      "locations": [
        {
          "line": 3,
          "column": 3
        }
      ],
      "path": [
        "client_upload_document"
      ]
    }
  ],
  "data": {
    "client_upload_document": null
  }
}
```

---
