# Entry Client Personal Details

> This API is for to store client personal details

---

### GRAPHQL

---

## Headers

| Fields         | Type   | Description                 | Example                                        | Data Management |
| -------------- | ------ | --------------------------- | ---------------------------------------------- | --------------- |
| URL            | string |                             | `{base_url}/graphql/fineract-interface-service`|                 |
| GRAPHQL        | string |                             | **MUTATION**                                   |                 |
| content-type   | string | JSON                        | **application/json**                           |                 |
| x-access-token | string | session token with validity | **token**                                      | user token      |

---

## Request

```json
mutation
{
  entry_client_personal_details( 
    PANnumber:"BTKPS9952F",
    aadhaar:"781234567890",
    paysack_company_id:"5fbcb4821ffc646d0433b1b3")
    {  
      resourceId
      success
    }
  
}


```

---

## Request Parameters

| Fields              | Type   | Description                  | Required                     |
| ------------------- | ------ | -----------------------------| ---------------------------- |
| PANnumber           | string | PAN number of client         |  <div align="center">✔</div> |
| aadhaar             | string | Aadhaar Number of client     |  <div align="center">✔</div> |
| paysack_company_id  | string | paysack company id of client |  <div align="center">✔</div> |

---

## Success

```json
{
  "data": {
    "entry_client_personal_details": {
      "resourceId": "602d19f08eeb8a2bf486b33d",
      "success":true
    }
  }
}




```

---

## Error

```json
{
  "errors": [
    {
      "statusCode": 500,
      "message": "Error in storing client personal details",
      "locations": [
        {
          "line": 3,
          "column": 3
        }
      ],
      "path": [
        "entry_client_personal_details"
      ]
    }
  ],
  "data": {
    "entry_client_personal_details": null
  }
}
```

---
