# Create Virtual Account

> This API is for to create virtual account for loan repayment by client

---

### GRAPHQL

---

## Headers

| Fields         | Type   | Description                 | Example                                        | Data Management |
| -------------- | ------ | --------------------------- | ---------------------------------------------- | --------------- |
| URL            | string |                             | `{base_url}/graphql/fineract-paysack-payments` |                 |
| GRAPHQL        | string |                             | **QUERY**                                      |                 |
| content-type   | string | JSON                        | **application/json**                           |                 |
| x-access-token | string | session token with validity | **token**                                      | user token      |

---

## Request

```json
query
{
  create_virtual_account_for_loanId(loanId:480)
  {  
    virtual_account_id,
    virtual_account_name,
    virtual_account_status,
    virtual_account_number,
    virtual_account_ifsc,
    success   
  }
  
}

```

---

## Request Parameters

| Fields   | Type   | Description                  | Required                     |
| -------- | ------ | -----------------------------| ---------------------------- |
| name     | string | name of the file             |  <div align="center">✔</div> |
| file     | string | file                         |  <div align="center">✔</div> |

---

## Success

```json
{
  "data": {
    "create_virtual_account_for_loanId": 
    {
      "virtual_account_id": "va_GqRROwgPGXyDKA",
      "virtual_account_name": "Mad Over Mobile Private Limited",
      "virtual_account_status": "active",
      "virtual_account_number": "1112220087978000",
      "virtual_account_ifsc": "RAZR0000001",
      "success": true
    }
  }
}


```

---

## Error

```json
{
  "errors": [
    {
      "statusCode": 500,
      "message": "Error in creating virtual account",
      "locations": [
        {
          "line": 3,
          "column": 3
        }
      ],
      "path": [
        "create_virtual_account_for_loanId"
      ]
    }
  ],
  "data": {
    "create_virtual_account_for_loanId": null
  }
}
```

---
