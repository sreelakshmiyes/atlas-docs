# Entry Client Bank Details

> This API is for to store client bank details

---

### GRAPHQL

---

## Headers

| Fields         | Type   | Description                 | Example                                        | Data Management |
| -------------- | ------ | --------------------------- | ---------------------------------------------- | --------------- |
| URL            | string |                             | `{base_url}/graphql/fineract-interface-service`|                 |
| GRAPHQL        | string |                             | **MUTATION**                                   |                 |
| content-type   | string | JSON                        | **application/json**                           |                 |
| x-access-token | string | session token with validity | **token**                                      | user token      |

---

## Request

```json
mutation
{
  entry_client_bank_details
  (bankName:"Federal Bank",
  bankIFSCCode:"FDRL019999",
  bankAccountName:"Sreelakshmi Sunil",
  accountNo:"10090891006754")
  {  
    resourceId
    success
  }
  
}


```

---

## Request Parameters

| Fields          | Type   | Description                  | Required                     |
| --------------- | ------ | -----------------------------| ---------------------------- |
| bankName        | string | name of the bank             |  <div align="center">✔</div> |
| bankIFSCCode    | string | ifsc code of the bank        |  <div align="center">✔</div> |
| bankAccountName | string | client name in bank records  |  <div align="center">✔</div> |
| accountNo       | string | client account number        |  <div align="center">✔</div> |


---

## Success

```json
{
  "data": {
    "entry_client_bank_details": {
      "resourceId": "602d19f08eeb8a2bf486b33d",
      "success":true
    }
  }
}



```

---

## Error

```json
{
  "errors": [
    {
      "statusCode": 500,
      "message": "Error in storing client bank details",
      "locations": [
        {
          "line": 3,
          "column": 3
        }
      ],
      "path": [
        "entry_client_bank_details"
      ]
    }
  ],
  "data": {
    "entry_client_bank_details": null
  }
}
```

---
