# Admin Fetch RazorpayX Balance

> This API is for fetching RazorpayX wallet balance

---

### GRAPHQL

---

## Headers

| Fields         | Type   | Description                 | Example                                         | Data Management |
| -------------- | ------ | --------------------------- | ----------------------------------------------- | --------------- |
| URL            | string |                             | `{base_url}/graphql/admin-service`              |                 |
| GRAPHQL        | string |                             | **QUERY**                                       |                 |
| content-type   | string | JSON                        | **application/json**                            |                 |
| x-access-token | string | session token with validity | **token**                                       | admin token     |

---

## Request

```json
query{
    admin_fetch_razorpayx_balance{
        balance
    }
}

```
---

## Success

```json
{
  "data": {
    "admin_fetch_razorpayx_balance": {
      "balance": 19626
    }
  }
}


```

---

## Error

```json
{
  "errors": [
    {
      "statusCode": 500,
      "message": "Error in getting RazorpayX Wallet Balance",
      "locations": [
        {
          "line": 3,
          "column": 3
        }
      ],
      "path": [
        "admin_fetch_razorpayx_balance"
      ]
    }
  ],
  "data": {
    "admin_fetch_razorpayx_balance": null
  }
}

```

---
