# Create Paysack Connection

> This API is for creating a Paysack connection for a company

---

### GRAPHQL

---

## Headers

| Fields       | Type   | Description | Example                                          | Data Management |
| ------------ | ------ | ----------- | ------------------------------------------------ | --------------- |
| URL          | string |             | `{base_url}/graphql/client-registration-service` |                 |
| GRAPHQL      | string |             | **MUTATION**                     |               |                 |
| content-type | string | JSON        | **application/json**             |               |                 |

---

## Request

```json
mutation{
  create_paysack_connection(
  company_id:"59de2820f31cc90001d65bb0",
  company_admin_token:"XXXXXX "
  )
  {
    _id,
    success
  }
 
} 

```

---

## Request Parameters

| Fields               | Type   | Description                  | Required                      |
| ---------------------| ------ | ---------------------------- | ----------------------------- |
| company_id           | string |  company_id                  |  <div align="center">✔</div>  |
| company_admin_token  | string | company_admin_token          |  <div align="center">✔</div>  |

---

## Success

```json
{
  "data": {
	"create_paysack_connection": {
    	"_id": "601b82462bf5dd87a6b07022",
    	"success": true
	  }
  }
}


```

---

## Error

```json
{
  "errors": [
    {
      "statusCode": 500,
      "message": "Error in creating paysack connection",
      "locations": [
        {
          "line": 3,
          "column": 3
        }
      ],
      "path": [
        "create_paysack_connection"
      ]
    }
  ],
  "data": {
    "create_paysack_connection": null
  }

```

\
\


