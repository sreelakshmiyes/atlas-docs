# Create CBS Connection

> This API is for creating a CBS connection for a user

---

### GRAPHQL

---

## Headers

| Fields       | Type   | Description | Example                                          | Data Management |
| ------------ | ------ | ----------- | ------------------------------------------------ | --------------- |
| URL          | string |             | `{base_url}/graphql/client-registration-service` |                 |
| GRAPHQL      | string |             | **MUTATION**                     |               |                 |
| content-type | string | JSON        | **application/json**             |               |                 |

---

## Request

```json
mutation{
    create_cbs_connection(
    base_url:"https://cbsdemo.kred.cloud/fineract-provider/api/v1",
	username: "Kred",
	password: "XXXXX",
    company:"5fbcb4821ffc64855233b1b1",tenant_id:"default")
    {
        _id,
        success
    } 
}
 

```

---

## Request Parameters

| Fields   | Type   | Description                  | Required                      |
| -------- | ------ | ---------------------------- | ----------------------------- |
| base_url | string | base url of cbs              |  <div align="center">✔</div> |
| username | string | username of admin            |  <div align="center">✔</div> |
| password | string | password of the admin        |  <div align="center">✔</div> |
| company  | string | paysack company for the user |  <div align="center">✔</div> |

---

## Success

```json
{
  "data": 
  {
	"create_cbs_connection": 
    {
  	"_id": "60001ba79451ec1ac87b9b12",
  	"success": true
	}
  }
}

```

---

## Error

```json
{
  "errors": [
    {
      "statusCode": 500,
      "message": "Error in creating cbs connection",
      "locations": [
        {
          "line": 3,
          "column": 3
        }
      ],
      "path": [
        "create_cbs_connection"
      ]
    }
  ],
  "data": {
    "create_cbs_connection": null
  }

```

\
\


