# Admin List Clients

> This API is for admin to list all clients

---

### GRAPHQL

---

## Headers

| Fields         | Type   | Description                 | Example                                         | Data Management |
| -------------- | ------ | --------------------------- | ----------------------------------------------- | --------------- |
| URL            | string |                             | `{base_url}/graphql/admin-service`              |                 |
| GRAPHQL        | string |                             | **QUERY**                                       |                 |
| content-type   | string | JSON                        | **application/json**                            |                 |
| x-access-token | string | session token with validity | **token**                                       | admin token     |

---

## Request

```json
query{
  admin_list_clients(limit:3,offset:0)
  {  
    client_list{
      id,
      active,
      displayName,
      mobileNo
    }
    hasMore  
  }
  
}

```
---

## Request Parameters

| Fields                   | Type    | Description                      | Required                    |
| ------------------------ | ------- | -------------------------------- | --------------------------- |
| limit                    | integer | client record limit for page     | <div align="center">✔</div> |
| offset                   | integer | page offset                      | <div align="center">✔</div> |


---

## Success

```json
{
  "data": {
    "admin_list_clients": {
      "client_list": [
        {
          "id": 162,
          "active": true,
          "displayName": "Alex",
          "mobileNo": "8086383171"
        },
        {
          "id": 163,
          "active": true,
          "displayName": "Sachin",
          "mobileNo": "9037580972"
        }
      ],
      "hasMore": false
    }
  }
}



```

---

## Error

```json
{
  "errors": [
    {
      "statusCode": 500,
      "message": "Error in getting the client details",
      "locations": [
        {
          "line": 3,
          "column": 3
        }
      ],
      "path": [
        "admin_fetch_client_details"
      ]
    }
  ],
  "data": {
    "admin_fetch_client_details": null
  }
}

```

---
