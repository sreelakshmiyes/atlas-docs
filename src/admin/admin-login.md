# Admin Login

> This API is for admin login

---

### GRAPHQL

---

## Headers

| Fields         | Type   | Description                 | Example                                         | Data Management |
| -------------- | ------ | --------------------------- | ----------------------------------------------- | --------------- |
| URL            | string |                             | `{base_url}/graphql/admin-service`              |                 |
| GRAPHQL        | string |                             | **MUTATION**                                    |                 |
| content-type   | string | JSON                        | **application/json**                            |                 |
| x-access-token | string | session token with validity | **token**                                       | admin token     |

---

## Request

```json
mutation{
  admin_login(
    password:"QwertyXXX",
    email:"nickXXX@gmail.com")
    {    
        token
    }
  
}

```
---

## Request Parameters

| Fields               | Type   | Description                  | Required                      |
| ---------------------| ------ | ---------------------------- | ----------------------------- |
| email                | string |  email                       |  <div align="center">✔</div>  |
| password             | string | password                     |  <div align="center">✔</div>  |


---

## Success

```json
{
  "data": {
    "admin_login": {
      "token": "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9......XXXXXX"
    }
  }
}


```

---

## Error

```json
{
  "errors": [
    {
      "statusCode": 500,
      "message": "Error in getting the admin details",
      "locations": [
        {
          "line": 3,
          "column": 3
        }
      ],
      "path": [
        "admin_login"
      ]
    }
  ],
  "data": {
    "admin_login": null
  }

```

---
