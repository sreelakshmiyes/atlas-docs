# Admin Fetch Client Details

> This API is for to get details of each client when given the client id

---

### GRAPHQL

---

## Headers

| Fields         | Type   | Description                 | Example                                         | Data Management |
| -------------- | ------ | --------------------------- | ----------------------------------------------- | --------------- |
| URL            | string |                             | `{base_url}/graphql/admin-service`              |                 |
| GRAPHQL        | string |                             | **QUERY**                                       |                 |
| content-type   | string | JSON                        | **application/json**                            |                 |
| x-access-token | string | session token with validity | **token**                                       | admin token     |

---

## Request

```json
query{
  admin_fetch_client_details(clientId:161)
  {
    firstname,
    lastname, 
    gender, 
    mobile, 
    dateofbirth,
    email, 
    company_id, 
    pan, 
    aadhaar, 
    employer_name,
    monthly_salary,
    credit_score,
    credit_score_percentage,
    loan_id,
    profile_approval_status,
    document_upload,
    loan_approval_status 
  }
}


```

---

## Success

```json
{
  "data": {
    "admin_fetch_client_details": {
      "firstname": "Lalitha",
      "lastname": "V N",
      "gender": "Female",
      "mobile": "9387211575",
      "dateofbirth": "24-4-1973",
      "email": "lalithavn@gmail.com",
      "company_id": "5fbcb4821ffc64855233b1b1",
      "pan": null,
      "aadhaar": "1122334455667",
      "employer_name": "Kred",
      "monthly_salary": 15000,
      "credit_score": "600",
      "credit_score_percentage": "70",
      "loan_id": null,
      "profile_approval_status": "pending",
      "document_upload": "pending",
      "loan_approval_status": "not_submitted"
    }
  }
}


```

---

## Error

```json
{
  "errors": [
    {
      "statusCode": 500,
      "message": "Error in getting the client details",
      "locations": [
        {
          "line": 3,
          "column": 3
        }
      ],
      "path": [
        "admin_fetch_client_details"
      ]
    }
  ],
  "data": {
    "admin_fetch_client_details": null
  }

```

---
