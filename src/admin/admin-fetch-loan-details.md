# Admin Fetch Loan Details

> This API is for admin to fetch loan details

---

### GRAPHQL

---

## Headers

| Fields         | Type   | Description                 | Example                                         | Data Management |
| -------------- | ------ | --------------------------- | ----------------------------------------------- | --------------- |
| URL            | string |                             | `{base_url}/graphql/admin-service`              |                 |
| GRAPHQL        | string |                             | **QUERY**                                       |                 |
| content-type   | string | JSON                        | **application/json**                            |                 |
| x-access-token | string | session token with validity | **token**                                       | admin token     |

---

## Request

```json
query{
    admin_fetch_loan_details(loanId:510)
    {
        id,
        accountNumber,
        status{
          id,
          code,
          value,
          pendingApproval,
          waitingForDisbursal,
          active,
          closedObligationsMet,
          closedWrittenOff,
          closedRescheduled,
          closed,
          overpaid
		},
        clientId,
        clientAccountNo,
        clientName,
        clientOfficeId,
        loanProductId,
        loanProductName,
        loanProductDescription,
        isLoanProductLinkedToFloatingRate,
        loanType{
   			id,
    		code,
   			value
  		},
        currency{
            code,
            name,
            decimalPlaces,
            inMultiplesOf,
            displaySymbol,
        	nameCode,
            displayLabel
        },
        principal,
        approvedPrincipal,
        proposedPrincipal,
        termFrequency,
        termPeriodFrequencyType{
            id,
            code,
            value
        },
        numberOfRepayments,
        repaymentEvery,
        repaymentFrequencyType{
            id,
            code,
            value,
        },
        interestRatePerPeriod,
        interestRateFrequencyType{
            id,
            code,
            value
		},
        annualInterestRate,
        isFloatingInterestRate,
        amortizationType{
            id,
            code,
            value
        },
        interestType{
            id,
            code,
            value
        },
        interestCalculationPeriodType{
            id,
            code,
            value
        },
        allowPartialPeriodInterestCalcualtion,
        transactionProcessingStrategyId,
        transactionProcessingStrategyName,
        syncDisbursementWithMeeting,
        timeline{
            submittedOnDate,
            submittedByUsername,
            submittedByFirstname,
            submittedByLastname,
            approvedOnDate,
            approvedByUsername,
            approvedByFirstname,
            approvedByLastname,
            expectedDisbursementDate,
            actualDisbursementDate,
            disbursedByUsername,
            disbursedByFirstname,
            disbursedByLastname,
            expectedMaturityDate
        },
        summary{
            currency{
    			code,
      			name,
   				decimalPlaces,
     			inMultiplesOf,
   			    displaySymbol,
      		    nameCode,
    			displayLabel
  			},
            principalDisbursed,
            principalPaid,
            principalWrittenOff,
            principalOutstanding,
            principalOverdue,
            interestCharged,
            interestPaid,
            interestWaived,
            interestWrittenOff,
            interestOutstanding,
            interestOverdue,
            feeChargesCharged,
            feeChargesDueAtDisbursementCharged,
            feeChargesPaid,
            feeChargesWaived,
            feeChargesWrittenOff,
            feeChargesOutstanding,
            feeChargesOverdue,
            penaltyChargesCharged,
            penaltyChargesPaid,
            penaltyChargesWaived,
            penaltyChargesWrittenOff,
            penaltyChargesOutstanding,
            penaltyChargesOverdue,
            totalExpectedRepayment,
            totalRepayment,
            totalExpectedCostOfLoan,
            totalCostOfLoan,
            totalWaived,
            totalWrittenOff,
            totalOutstanding,
            totalOverdue,
            overdueSinceDate
        },
        repaymentSchedule{
            currency{
                code,
                name,
                decimalPlaces,
                inMultiplesOf,
                displaySymbol,
                nameCode,
                displayLabel
            },
            loanTermInDays,
            totalPrincipalDisbursed,
            totalPrincipalExpected,
            totalPrincipalPaid,
            totalInterestCharged,
            totalFeeChargesCharged,
            totalPenaltyChargesCharged,
            totalWaived,
            totalWrittenOff,
            totalRepaymentExpected,
            totalRepayment,
            totalPaidInAdvance,
            totalPaidLate,
            totalOutstanding,
            periods{
                period,
                fromDate,
                dueDate,
                obligationsMetOnDate,
                complete,
                daysInPeriod,
                principalOriginalDue,
                principalDue,
                principalPaid,
                principalWrittenOff,
                principalOutstanding,
                principalLoanBalanceOutstanding,
                interestOriginalDue,
                interestDue,
                interestPaid,
                interestWaived,
                interestWrittenOff,
                interestOutstanding,
                feeChargesDue,
                feeChargesPaid,
                feeChargesWaived,
                feeChargesWrittenOff,
                feeChargesOutstanding,
                penaltyChargesDue,
                penaltyChargesPaid,
                penaltyChargesWaived,
                penaltyChargesWrittenOff,
                penaltyChargesOutstanding,
                totalOriginalDueForPeriod,
                totalDueForPeriod,
                totalPaidForPeriod,
                totalPaidInAdvanceForPeriod,
                totalPaidLateForPeriod,
                totalWaivedForPeriod,
                totalWrittenOffForPeriod,
                totalOutstandingForPeriod,
                totalOverdue,
                totalActualCostOfLoanForPeriod,
                totalInstallmentAmountForPeriod
            }
        },
        transactions{
            id,
            officeId,
            officeName,
            type{
                id
                code,
                value,
                disbursement,
                repaymentAtDisbursement,
                repayment,
                contra,
                waiveInterest,
                waiveCharges,
                accrual,
                writeOff,
                recoveryRepayment,
                initiateTransfer,
                approveTransfer,
                withdrawTransfer,
                rejectTransfer,
                chargePayment,
                refund,
                refundForActiveLoans
            },    			
            date,
            currency{
                code,
                name,
                decimalPlaces,
                inMultiplesOf,
                displaySymbol,
                nameCode,
                displayLabel
            },
            paymentDetailData
            {
                id,
                paymentType{
                    id,
                     name
     		    },
                accountNumber,
                checkNumber,
                routingCode,
                receiptNumber,
                bankNumber
            },
            amount,
            principalPortion,
            interestPortion,
            feeChargesPortion,
            penaltyChargesPortion,
            overpaymentPortion,
    	    unrecognizedIncomePortion,
            outstandingLoanBalance,
            submittedOnDate,
            manuallyReversed
        },
        notes{
            id,
            clientId,
            loanId,
            noteType{
                id,
                code,
                value
            },
            note,
            createdById,
            createdByUsername,
            createdOn,
            updatedById,
            updatedByUsername,
            updatedOn
        },
        disbursementDetails,
        feeChargesAtDisbursementCharged,
        loanProductCounter,
        multiDisburseLoan,
        canDefineInstallmentAmount,
        canDisburse,
        emiAmountVariations,
        canUseForTopup,
        isTopup,
        closureLoanId,
        inArrears,
        isNPA,
        overdueCharges,
        daysInMonthType{
            id,
            code,
            value
        },
        daysInYearType{
            id,
            code,
            value
        },
        isInterestRecalculationEnabled,
        createStandingInstructionAtDisbursement,
        paidInAdvance{
            paidInAdvance
        },
        isVariableInstallmentsAllowed,
        minimumGap,
        maximumGap,
        isEqualAmortization
    }
}

```
---

## Request Parameters

| Fields                   | Type    | Description                      | Required                    |
| ------------------------ | ------- | -------------------------------- | --------------------------- |
| loanId                   | integer | loanId                           | <div align="center">✔</div> |


---

## Success

```json
{
  "data": {
    "admin_loans_fetch_repayment_schedule": {
      "id": 510,
      "accountNumber": null,
      "status": {
        "id": 300,
        "code": "loanStatusType.active",
        "value": "Active",
        "pendingApproval": false,
        "waitingForDisbursal": false,
        "active": true,
        "closedObligationsMet": false,
        "closedWrittenOff": false,
        "closedRescheduled": false,
        "closed": false,
        "overpaid": false
      },
      "clientId": 163,
      "clientAccountNo": "Head Offic000000163",
      "clientName": "Sachin T S",
      "clientOfficeId": 1,
      "loanProductId": 14,
      "loanProductName": "KredCBS_POC_loan_product",
      "loanProductDescription": "Credit line",
      "isLoanProductLinkedToFloatingRate": "false",
      "loanType": {
        "id": 1,
        "code": "accountType.individual",
        "value": "Individual"
      },
      "currency": {
        "code": "INR",
        "name": "Indian Rupee",
        "decimalPlaces": 2,
        "inMultiplesOf": 1,
        "displaySymbol": "₹",
        "nameCode": "currency.INR",
        "displayLabel": "Indian Rupee (₹)"
      },
      "principal": 10,
      "approvedPrincipal": 10,
      "proposedPrincipal": 10,
      "termFrequency": 3,
      "termPeriodFrequencyType": {
        "id": 0,
        "code": "termFrequency.periodFrequencyType.days",
        "value": "Days"
      },
      "numberOfRepayments": 3,
      "repaymentEvery": 1,
      "repaymentFrequencyType": {
        "id": 0,
        "code": "repaymentFrequency.periodFrequencyType.days",
        "value": "Days"
      },
      "interestRatePerPeriod": 365,
      "interestRateFrequencyType": {
        "id": 3,
        "code": "interestRateFrequency.periodFrequencyType.years",
        "value": "Per year"
      },
      "annualInterestRate": 365,
      "isFloatingInterestRate": false,
      "amortizationType": {
        "id": 1,
        "code": "amortizationType.equal.installments",
        "value": "Equal installments"
      },
      "interestType": {
        "id": 1,
        "code": "interestType.flat",
        "value": "Flat"
      },
      "interestCalculationPeriodType": {
        "id": 1,
        "code": "interestCalculationPeriodType.same.as.repayment.period",
        "value": "Same as repayment period"
      },
      "allowPartialPeriodInterestCalcualtion": false,
      "transactionProcessingStrategyId": 1,
      "transactionProcessingStrategyName": "Penalties, Fees, Interest, Principal order",
      "syncDisbursementWithMeeting": false,
      "timeline": {
        "submittedOnDate": [ 2021,5,11],
        "submittedByUsername": "kred",
        "submittedByFirstname": "App",
        "submittedByLastname": "Administrator",
        "approvedOnDate": [ 2021,5,11],
        "approvedByUsername": "kred",
        "approvedByFirstname": "App",
        "approvedByLastname": "Administrator",
        "expectedDisbursementDate": [2021,5,11],
        "actualDisbursementDate": [2021,5,11],
        "disbursedByUsername": "kred",
        "disbursedByFirstname": "App",
        "disbursedByLastname": "Administrator",
        "expectedMaturityDate": [2021,5,14]
      },
      "summary": {
        "currency": {
          "code": "INR",
          "name": "Indian Rupee",
          "decimalPlaces": 2,
          "inMultiplesOf": 1,
          "displaySymbol": "₹",
          "nameCode": "currency.INR",
          "displayLabel": "Indian Rupee (₹)"
        },
        "principalDisbursed": 10,
        "principalPaid": 0,
        "principalWrittenOff": 0,
        "principalOutstanding": 10,
        "principalOverdue": 10,
        "interestCharged": 0.3,
        "interestPaid": 0,
        "interestWaived": 0,
        "interestWrittenOff": 0,
        "interestOutstanding": 0.3,
        "interestOverdue": 0.3,
        "feeChargesCharged": 0,
        "feeChargesDueAtDisbursementCharged": 0,
        "feeChargesPaid": 0,
        "feeChargesWaived": 0,
        "feeChargesWrittenOff": 0,
        "feeChargesOutstanding": 0,
        "feeChargesOverdue": 0,
        "penaltyChargesCharged": 0,
        "penaltyChargesPaid": 0,
        "penaltyChargesWaived": 0,
        "penaltyChargesWrittenOff": 0,
        "penaltyChargesOutstanding": 0,
        "penaltyChargesOverdue": 0,
        "totalExpectedRepayment": 10.3,
        "totalRepayment": 0,
        "totalExpectedCostOfLoan": 0.3,
        "totalCostOfLoan": 0,
        "totalWaived": 0,
        "totalWrittenOff": 0,
        "totalOutstanding": 10.3,
        "totalOverdue": 10.3,
        "overdueSinceDate": [2021,5,12]
      },
      "repaymentSchedule": {
        "currency": {
          "code": "INR",
          "name": "Indian Rupee",
          "decimalPlaces": 2,
          "inMultiplesOf": 1,
          "displaySymbol": "₹",
          "nameCode": "currency.INR",
          "displayLabel": "Indian Rupee (₹)"
        },
        "loanTermInDays": 3,
        "totalPrincipalDisbursed": 10,
        "totalPrincipalExpected": 10,
        "totalPrincipalPaid": 0,
        "totalInterestCharged": 0.3,
        "totalFeeChargesCharged": 0,
        "totalPenaltyChargesCharged": 0,
        "totalWaived": 0,
        "totalWrittenOff": 0,
        "totalRepaymentExpected": 10.3,
        "totalRepayment": 0,
        "totalPaidInAdvance": 0,
        "totalPaidLate": 0,
        "totalOutstanding": 10.3,
        "periods": [
          {
            "period": null,
            "fromDate": null,
            "dueDate": [2021,5,11],
            "obligationsMetOnDate": null,
            "complete": null,
            "daysInPeriod": null,
            "principalOriginalDue": null,
            "principalDue": null,
            "principalPaid": null,
            "principalWrittenOff": null,
            "principalOutstanding": null,
            "principalLoanBalanceOutstanding": 10,
            "interestOriginalDue": null,
            "interestDue": null,
            "interestPaid": null,
            "interestWaived": null,
            "interestWrittenOff": null,
            "interestOutstanding": null,
            "feeChargesDue": 0,
            "feeChargesPaid": 0,
            "feeChargesWaived": null,
            "feeChargesWrittenOff": null,
            "feeChargesOutstanding": null,
            "penaltyChargesDue": null,
            "penaltyChargesPaid": null,
            "penaltyChargesWaived": null,
            "penaltyChargesWrittenOff": null,
            "penaltyChargesOutstanding": null,
            "totalOriginalDueForPeriod": 0,
            "totalDueForPeriod": 0,
            "totalPaidForPeriod": 0,
            "totalPaidInAdvanceForPeriod": null,
            "totalPaidLateForPeriod": null,
            "totalWaivedForPeriod": null,
            "totalWrittenOffForPeriod": null,
            "totalOutstandingForPeriod": null,
            "totalOverdue": null,
            "totalActualCostOfLoanForPeriod": 0,
            "totalInstallmentAmountForPeriod": null
          },
          {
            "period": 1,
            "fromDate": [2021,5,11],
            "dueDate": [2021,5,12],
            "obligationsMetOnDate": null,
            "complete": false,
            "daysInPeriod": 1,
            "principalOriginalDue": 3.33,
            "principalDue": 3.33,
            "principalPaid": 0,
            "principalWrittenOff": 0,
            "principalOutstanding": 3.33,
            "principalLoanBalanceOutstanding": 6.67,
            "interestOriginalDue": 0.1,
            "interestDue": 0.1,
            "interestPaid": 0,
            "interestWaived": 0,
            "interestWrittenOff": 0,
            "interestOutstanding": 0.1,
            "feeChargesDue": 0,
            "feeChargesPaid": 0,
            "feeChargesWaived": 0,
            "feeChargesWrittenOff": 0,
            "feeChargesOutstanding": 0,
            "penaltyChargesDue": 0,
            "penaltyChargesPaid": 0,
            "penaltyChargesWaived": 0,
            "penaltyChargesWrittenOff": 0,
            "penaltyChargesOutstanding": 0,
            "totalOriginalDueForPeriod": 3.43,
            "totalDueForPeriod": 3.43,
            "totalPaidForPeriod": 0,
            "totalPaidInAdvanceForPeriod": 0,
            "totalPaidLateForPeriod": 0,
            "totalWaivedForPeriod": 0,
            "totalWrittenOffForPeriod": 0,
            "totalOutstandingForPeriod": 3.43,
            "totalOverdue": 3.43,
            "totalActualCostOfLoanForPeriod": 0.1,
            "totalInstallmentAmountForPeriod": 3.43
          },
          {
            "period": 2,
            "fromDate": [2021,5,12],
            "dueDate": [2021,5,13],
            "obligationsMetOnDate": null,
            "complete": false,
            "daysInPeriod": 1,
            "principalOriginalDue": 3.33,
            "principalDue": 3.33,
            "principalPaid": 0,
            "principalWrittenOff": 0,
            "principalOutstanding": 3.33,
            "principalLoanBalanceOutstanding": 3.34,
            "interestOriginalDue": 0.1,
            "interestDue": 0.1,
            "interestPaid": 0,
            "interestWaived": 0,
            "interestWrittenOff": 0,
            "interestOutstanding": 0.1,
            "feeChargesDue": 0,
            "feeChargesPaid": 0,
            "feeChargesWaived": 0,
            "feeChargesWrittenOff": 0,
            "feeChargesOutstanding": 0,
            "penaltyChargesDue": 0,
            "penaltyChargesPaid": 0,
            "penaltyChargesWaived": 0,
            "penaltyChargesWrittenOff": 0,
            "penaltyChargesOutstanding": 0,
            "totalOriginalDueForPeriod": 3.43,
            "totalDueForPeriod": 3.43,
            "totalPaidForPeriod": 0,
            "totalPaidInAdvanceForPeriod": 0,
            "totalPaidLateForPeriod": 0,
            "totalWaivedForPeriod": 0,
            "totalWrittenOffForPeriod": 0,
            "totalOutstandingForPeriod": 3.43,
            "totalOverdue": 3.43,
            "totalActualCostOfLoanForPeriod": 0.1,
            "totalInstallmentAmountForPeriod": 3.43
          },
            ......
        "transactions": [
        {
          "id": 1124,
          "officeId": 1,
          "officeName": "Head Office",
          "type": {
            "id": 1,
            "code": "loanTransactionType.disbursement",
            "value": "Disbursement",
            "disbursement": true,
            "repaymentAtDisbursement": false,
            "repayment": false,
            "contra": false,
            "waiveInterest": false,
            "waiveCharges": false,
            "accrual": false,
            "writeOff": false,
            "recoveryRepayment": false,
            "initiateTransfer": false,
            "approveTransfer": false,
            "withdrawTransfer": false,
            "rejectTransfer": false,
            "chargePayment": false,
            "refund": false,
            "refundForActiveLoans": false
          },
          "date": [2021,5,11],
          "currency": {
            "code": "INR",
            "name": "Indian Rupee",
            "decimalPlaces": 2,
            "inMultiplesOf": 1,
            "displaySymbol": "₹",
            "nameCode": "currency.INR",
            "displayLabel": "Indian Rupee (₹)"
          },
          "paymentDetailData": {
            "id": 1124,
            "paymentType": {
              "id": 1,
              "name": "Cash"
            },
            "accountNumber": "",
            "checkNumber": "12345",
            "routingCode": "",
            "receiptNumber": "",
            "bankNumber": ""
          },
          "amount": 10,
          "principalPortion": 0,
          "interestPortion": 0,
          "feeChargesPortion": 0,
          "penaltyChargesPortion": 0,
          "overpaymentPortion": 0,
          "unrecognizedIncomePortion": 0,
          "outstandingLoanBalance": 10,
          "submittedOnDate": [2021,5,11],
          "manuallyReversed": false
        },
        {
          "id": 1125,
          "officeId": 1,
          "officeName": "Head Office",
          "type": {
            "id": 10,
            "code": "loanTransactionType.accrual",
            "value": "Accrual",
            "disbursement": false,
            "repaymentAtDisbursement": false,
            "repayment": false,
            "contra": false,
            "waiveInterest": false,
            "waiveCharges": false,
            "accrual": true,
            "writeOff": false,
            "recoveryRepayment": false,
            "initiateTransfer": false,
            "approveTransfer": false,
            "withdrawTransfer": false,
            "rejectTransfer": false,
            "chargePayment": false,
            "refund": false,
            "refundForActiveLoans": false
          },
          "date": [2021,5,11],
          "currency": {
            "code": "INR",
            "name": "Indian Rupee",
            "decimalPlaces": 2,
            "inMultiplesOf": 1,
            "displaySymbol": "₹",
            "nameCode": "currency.INR",
            "displayLabel": "Indian Rupee (₹)"
          },
          "paymentDetailData": null,
          "amount": 0.3,
          "principalPortion": 0,
          "interestPortion": 0.3,
          "feeChargesPortion": 0,
          "penaltyChargesPortion": 0,
          "overpaymentPortion": 0,
          "unrecognizedIncomePortion": 0,
          "outstandingLoanBalance": 0,
          "submittedOnDate": [2021,5,11],
          "manuallyReversed": false
        }
      ],
      "notes": [
        {
          "id": 879,
          "clientId": 163,
          "loanId": 510,
          "noteType": {
            "id": 200,
            "code": "noteType.loan",
            "value": "Loan note"
          },
          "note": "loan approved",
          "createdById": 1,
          "createdByUsername": "kred",
          "createdOn": "1620738453000",
          "updatedById": 1,
          "updatedByUsername": "kred",
          "updatedOn": "1620738453000"
        },
        {
          "id": 880,
          "clientId": 163,
          "loanId": 510,
          "noteType": {
            "id": 200,
            "code": "noteType.loan",
            "value": "Loan note"
          },
          "note": "disbursed",
          "createdById": 1,
          "createdByUsername": "kred",
          "createdOn": "1620738453000",
          "updatedById": 1,
          "updatedByUsername": "kred",
          "updatedOn": "1620738453000"
        }
      ],
      "disbursementDetails": [],
      "feeChargesAtDisbursementCharged": 0,
      "loanProductCounter": 8,
      "multiDisburseLoan": false,
      "canDefineInstallmentAmount": false,
      "canDisburse": false,
      "emiAmountVariations": [],
      "canUseForTopup": false,
      "isTopup": false,
      "closureLoanId": 0,
      "inArrears": true,
      "isNPA": false,
      "overdueCharges": [],
      "daysInMonthType": {
        "id": 1,
        "code": "DaysInMonthType.actual",
        "value": "Actual"
      },
      "daysInYearType": {
        "id": 1,
        "code": "DaysInYearType.actual",
        "value": "Actual"
      },
      "isInterestRecalculationEnabled": false,
      "createStandingInstructionAtDisbursement": false,
      "paidInAdvance": {
        "paidInAdvance": 0
      },
      "isVariableInstallmentsAllowed": false,
      "minimumGap": 0,
      "maximumGap": 0,
      "isEqualAmortization": false
    }
  }
}  


```

---

## Error

```json
{
  "errors": [
    {
      "statusCode": 500,
      "message": "Error in fetching loan details",
      "locations": [
        {
          "line": 3,
          "column": 3
        }
      ],
      "path": [
        "admin_fetch_loan_details"
      ]
    }
  ],
  "data": {
    "admin_fetch_loan_details": null
  }
}

```

---
