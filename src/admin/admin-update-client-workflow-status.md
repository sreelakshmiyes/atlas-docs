# Admin Update Client Workflow Status

> This API is for admin to update client workflow status

---

### GRAPHQL

---

## Headers

| Fields       | Type   | Description | Example                                         | Data Management |
| ------------ | ------ | ----------- | ----------------------------------------------- | --------------- |
| URL          | string |             | `{base_url}/graphql/admin-service`              |                 |
| GRAPHQL      | string |             | **MUTATION**                                    |                 |
| content-type | string | JSON        | **application/json**                            |                 |

---

## Request

```json
mutation{
admin_update_client_workflow_status(
    client_id:161,
    document_upload:"pending",
    bank_details:"pending",
    loan_approval_status:"not_submitted"
    profile_approval_status:"pending")
    {
        loan_application_id 
    }
}

```

---

## Request Parameters

| Fields                   | Type    | Description                      | Required                    |
| ------------------------ | ------- | -------------------------------- | --------------------------- |
| client_id                | integer | client id                        | <div align="center">✔</div> |
| document_upload          | string  | document upload status           | <div align="center">✔</div> |
| bank_details             | string  | bank details upload status       | <div align="center">✔</div> |
| loan_approval_status     | string  | loan approval status             | <div align="center">✔</div> |
| profile_approval_status  | string  | profile approval status          | <div align="center">✔</div> |


---

## Success

```json
{
  "data": {
    "admin_update_client_workflow_status": {
      "loan_application_id": "604ee5537fec497d0dd9b5af"
    }
  }
}

```

---

## Error

```json
{
  "errors": [
    {
      "statusCode": 500,
      "message": "Error in updating CBS user's workflow status",
      "locations": [
        {
          "line": 3,
          "column": 3
        }
      ],
      "path": [
        "admin_update_client_workflow_status"
      ]
    }
  ],
  "data": {
    "admin_update_client_workflow_status": null
  }
}
```

